<?php
/**
 * This file is  Part of Another CSRF lib
 *
 * (c) 2020 Luri <luri@e.email>
 *
 ***********************************************************************************************************************
 *                                                       LICENCE
 ***********************************************************************************************************************
 *
 * Another CSRF lib is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the License, or any later version.
 *
 * Another CSRF lib is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with Another CSRF lib.
 * If not, see <https://www.gnu.org/licenses/>.
 *
 ***********************************************************************************************************************
 *
 * Another CSRF lib est un logiciel libre: vous pouvez le redistribuer et / ou le modifier sous les termes de la GNU General Public
 * License comme publié par la Free Software Foundation, version 3 de la licence ou toute version ultérieure.
 *
 * Another CSRF lib est distribué dans l'espoir qu'il sera utile, mais SANS AUCUNE GARANTIE; sans même la garantie implicite de
 * QUALITÉ MARCHANDE ou D'ADÉQUATION À UN USAGE PARTICULIER. Voir la Licence Publique Générale GNU pour plus de détails.
 *
 * Vous devriez avoir reçu une copie de la licence publique générale GNU avec Another CSRF lib.
 * Sinon, voir <https://www.gnu.org/licenses/>.
 *
 ***********************************************************************************************************************
 */
namespace Luri\ACSRFLib;

class Protect {
	/**
	 * Restrict token to a particular user-agent
	 */
	public const RESTRICT_USER_AGENT = 1;
	/**
	 * Restrict token to a particular IP
	 */
	public const RESTRICT_IP = 2;
	/**
	 * token expire after some time
	 */
	public const RESTRICT_VALID_TIME = 4;
	/**
	 * For use actual url
	 */
	public const ACTUAL_URL = "-47";
	/**
	 * Default valid time of an token (in s)
	 */
	public const DEFAULT_VALID_TIME = 600;




	/**
	 * Stockage des tokens afin qu'ils soyent sauvegardé entre chaque page.
	 * Ce doit être soit un tableau, soit une classe supportant ArrayAccess
	 * Les clé utilisé par cette classe commence par ACSRF_
	 *
	 * *************************************************************************
	 *
	 * Storage of Token for save between php page
	 * Must be an Array or an class who implements ArrayAccess
	 * Key used by this classesis start by ACSRF_
	 *
	 * *************************************************************************
	 *
	 * Typiquement / Typically : $_SESSION
	 *
	 * @var Array | ArrayAccess
	 */
	protected $storage;

	/**
	 * Serveur Info about visitor request
	 *
	 * @var ServerInfo
	 */
	protected $serverInfo;



	/**
	 * Instance lib
	 *
	 * @param \Array|\ArrayAccess $storage Storage of token. (Typically $_SESSION or an wrapper class around $_SESSION with ArrayAccess interface)
	 * @param \Luri\ACSRFLib\ServerInfo $serverInfo Info about request / user. Typically : new \Luri\ACSRFLib\ServerWrapper();
	 * @throws InvalidArgumentException if storage is not compatible with this classes
	 */
	public function __construct(&$storage, ServerInfo $serverInfo) {
		//Verify
		if (!is_array($storage) AND !($storage instanceof \ArrayAccess)) {
			throw new InvalidArgumentException('$storage must be an Array or an class who implement ArrayAccess');
		}

		//Save
		$this->storage = &$storage;
		$this->serverInfo = $serverInfo;

		//If necessary, format storage
		if (! isset($this->storage['ACSRF_TOKENS']) OR !is_array($this->storage['ACSRF_TOKENS'])) {
			$this->storage['ACSRF_TOKENS'] = [];
		}

		//Delete Expired Token
		$this->deleteExpiredToken();
	}

	/**
	 * Génère un jeton / Generate a token
	 *
	 * La validité du jeton peut être restreint à :
	 *
	 * - Une URL (par défautl, l'url actuel)
	 *
	 * - L'ip actuel du visiteur (par défault)
	 *
	 * - L'user agent actuel du visiteur (par défault)
	 *
	 * - Un formulaire particulier idenfié par la présence d'une paire clé-valeur (attribut html name/value)
	 *
	 * Le jeton est valide uniquement un certain temps, saud si cette option est désactivé dans les options.
	 * Chaque jeton est à usage unique.
	 *
	 *            ****************************
	 *
	 * Token can be restricted to :
	 *
	 *  - an URL (actual url by default)
	 *
	 *  - actual ip of visitor (by default)
	 *
	 *  - actuel user agent of visitor (by default)
	 *
	 *  - A particular form who is identified by an key-value paire (name/value html atttribut)
	 *
	 * Token is valid only for a time, expect of this option is desactivated (see $options parameter)
	 * A token can be use only one time
	 *
	 * @param array $idForm An key-value pair who identify form for new token is valid. Pass an empty array for no form restrict
	 * @param string $url URL for token is valid. For no url restrict, you must pass an empty string (Default: actual URL)
	 * @param int $validTime Valid Time of Token (Change option for no valid time)
	 * @param int $options Restrict option (Combinaison of self::RESTRICT_**)
	 * @return array Token : key = name, value = value of an input html tag (hidden type)
	 */
	public function generateToken(
		array $idForm = [],
		string $url = self::ACTUAL_URL,
		int $validTime = self::DEFAULT_VALID_TIME,
		int $options = self::RESTRICT_IP | self::RESTRICT_USER_AGENT | self::RESTRICT_VALID_TIME
		) : array {

		//Create token
		$token = new Token();
		$token->key = base64_encode(openssl_random_pseudo_bytes(40));
		$token->value = base64_encode(openssl_random_pseudo_bytes(72));
		$token->options = $options;
		$token->userAgent = $this->serverInfo->userAgent;
		$token->ip = $this->serverInfo->ip;

		if ($url == self::ACTUAL_URL) {
			$token->url = $this->serverInfo->url;
		} else {
			$token->url = $url;
		}

		if (! empty($idForm)) {
			$token->form = $idForm;
		}

		if ($options & self::RESTRICT_VALID_TIME) {
			//This token have a limited valid time
			if ($validTime <= 0) {
				$validTime = self::DEFAULT_VALID_TIME;
			}
			$token->validTime = time() + $validTime;
		}

		//Add token to storage
		$this->storage['ACSRF_TOKENS'][$token->key] = $token;

		//return key-value pair
		return [$token->key => $token->value];

	}

	/**
	 * Validate a request
	 *
	 * Search if an token is present and valid.
	 *
	 * A token can be use only one time.
	 *
	 * @param array $form value receipt by page (typically ($_GET or $_POST)
	 * @return bool True if this request is valid
	 */
	public function isValidRequest(array $form) : bool {
		if (empty($this->storage['ACSRF_TOKENS'])) {
			//pas le peine de chercher s'il n'y a aucun token d'enregistré
			//We have no token
			return false;
		}

		//Search token key
		//On prend la liste des clés des tokens et on la recherche dans le tableau passé en paramètre
		$tokensKeys = array_keys($this->storage['ACSRF_TOKENS']);
		$findedKey = null;
		foreach ($tokensKeys as $v) {
			if (isset($form[$v])) {
				$findedKey = $v;
				break;
			}
		}

		if (empty($findedKey)) {
			//No token key found
			return false;
		}

		//Verify token value
		if ($this->storage['ACSRF_TOKENS'][$findedKey]->value != $form[$findedKey]) {
			return false;
		}

		//Verify time validity
		if (($this->storage['ACSRF_TOKENS'][$findedKey]->options & self::RESTRICT_VALID_TIME) AND $this->storage['ACSRF_TOKENS'][$findedKey]->validTime < time()) {
			var_dump($this->storage['ACSRF_TOKENS'][$findedKey]);
			return false;
		}

		//Verify url
		if (! empty($this->storage['ACSRF_TOKENS'][$findedKey]->url) AND $this->storage['ACSRF_TOKENS'][$findedKey]->url != $this->serverInfo->url) {
			return false;
		}

		//Verify user agent
		if (($this->storage['ACSRF_TOKENS'][$findedKey]->options & self::RESTRICT_USER_AGENT) AND $this->storage['ACSRF_TOKENS'][$findedKey]->userAgent != $this->serverInfo->userAgent) {
			return false;
		}

		//Verify ip
		if (($this->storage['ACSRF_TOKENS'][$findedKey]->options & self::RESTRICT_IP) AND $this->storage['ACSRF_TOKENS'][$findedKey]->ip != $this->serverInfo->ip) {
			return false;
		}

		//verif form
		if (! empty($this->storage['ACSRF_TOKENS'][$findedKey]->form)) {
			$formKeyToVerify = key($this->storage['ACSRF_TOKENS'][$findedKey]->form);
			$formValueToVerify = current($this->storage['ACSRF_TOKENS'][$findedKey]->form);
			if (!isset($form[$formKeyToVerify])) {
				//Clé non présente
				return false;
			}
			if ($form[$formKeyToVerify] != $formValueToVerify) {
				//Mauvaise valeur
				return false;
			}
		}

		//If we arrive here, we have pass all verif, this request is valid !

		//Delete this token (one time use)
		unset($this->storage['ACSRF_TOKENS'][$findedKey]);

		return true;
	}

	/**
	 * Delete all existing Token
	 *
	 * @return void
	 */
	public function deleteAllTokens() : void {
		$this->storage['ACSRF_TOKENS'] = [];
	}

	/**
	 * Delete token who have time expired
	 *
	 * @return void
	 */
	private function deleteExpiredToken() : void {
		$tokenValid = [];
		foreach ($this->storage['ACSRF_TOKENS'] as $k => $v) {
			/* @var $v \Luri\ACSRFLib\Token  */
			if (!(($v->options & self::RESTRICT_VALID_TIME) AND $v->validTime < time())) {
				//Le seul cas où on rejette les jetons est celui où le token a une durée de validité et que cette durée de validité est dépassé. On prend donc tous les cas étant l'inverrse de celui ci. (Retournez à vos cours d'électronique et faites une table de vérité)
				$tokenValid[$k] = $v;
			}
		}

		$this->storage['ACSRF_TOKENS'] = $tokenValid;
	}
}
?>