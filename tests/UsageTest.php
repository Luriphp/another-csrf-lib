<?php
/**
 * This file is  Part of Another CSRF lib
 *
 * (c) 2020 Luri <luri@e.email>
 *
 ***********************************************************************************************************************
 *                                                       LICENCE
 ***********************************************************************************************************************
 *
 * Another CSRF lib is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the License, or any later version.
 *
 * Another CSRF lib is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with Another CSRF lib.
 * If not, see <https://www.gnu.org/licenses/>.
 *
 ***********************************************************************************************************************
 *
 * Another CSRF lib est un logiciel libre: vous pouvez le redistribuer et / ou le modifier sous les termes de la GNU General Public
 * License comme publié par la Free Software Foundation, version 3 de la licence ou toute version ultérieure.
 *
 * Another CSRF lib est distribué dans l'espoir qu'il sera utile, mais SANS AUCUNE GARANTIE; sans même la garantie implicite de
 * QUALITÉ MARCHANDE ou D'ADÉQUATION À UN USAGE PARTICULIER. Voir la Licence Publique Générale GNU pour plus de détails.
 *
 * Vous devriez avoir reçu une copie de la licence publique générale GNU avec Another CSRF lib.
 * Sinon, voir <https://www.gnu.org/licenses/>.
 *
 ***********************************************************************************************************************
 */
namespace Luri\ACSRFLib\Test;

use PHPUnit\Framework\TestCase;
use Luri\ACSRFLib\{
	Protect,
	ServerInfo
};


class UserListTest extends TestCase {

	/**
	 * This test see a simple example with defaut parameters.
	 *
	 * Here, following protection is active :
	 * - only valid for url where form is generated
	 * - only valid for ip of visitor
	 * - only valid for user agent of visitor
	 * - valid for 10 minutes
	 */
	public function testSimpleExample() {
		//
		// Test initialisation (you must no do this in prod)
		//
		$serverInfoInital = new ServerInfo();
		$serverInfoInital->ip = '10.10.10.10';
		$serverInfoInital->url = 'https://www.twitch.tv/chamallow_san/';
		$serverInfoInital->userAgent = 'console';

		$badIp = clone $serverInfoInital;
		$badIp->ip = '20.20.20.20';

		$badUrl = clone $serverInfoInital;
		$badUrl->url = 'https://www.youtube.com/watch?v=GALu3VyUhl0';

		$badUserAgent = clone  $serverInfoInital;
		$badUserAgent->userAgent  ='firefox';

		$storage = [];

		//
		// Part 1 - Before html form generation
		//
		/*
		 * For prod, you want create instance of protect like this :
		 * $protectLib = new Protect($_SESSION, new ServerWrapper());
		 *
		 * But here, we are in a phpunit test, so we make this :
		 */
		$protectLib = new Protect($storage, $serverInfoInital);

		$token = $protectLib->generateToken();

		//You must include token in html form like this :
		$echo = '<input type="hidden" name="' . key($token) . '" value="' . current($token) . '" />';

		// Below, we create a exemple form request. (In reality, browser make that)
		$tokenkey = key($token);
		$form = [
			$tokenkey => current($token),
			'name' => 'Parke',
			'firstname' => 'Ronan',
			'method' => 'Add Singer'
		];

		//
		// Part 2 - Next page, on receive form data
		//
		/*
		 * For prod, you want create instance of protect like this :
		 * $protectLib = new Protect($_SESSION, new ServerWrapper());
		 *
		 * But here, we are in a phpunit test, so we make this :
		 */
		$protectLib = new Protect($storage, $badIp);

		//In prod, make something like this :
		if ($protectLib->isValidRequest($form)) {
			//Token is valid, so you can process the request
		}

		//Here, we are in a phpunit test, so we make some test with bad data, after that we make a test who must mork.
		$this->assertFalse($protectLib->isValidRequest($form), 'Other IP test, token must not be valid');

		$protectLib = new Protect($storage, $badUrl);
		$this->assertFalse($protectLib->isValidRequest($form), 'Other URL test, token must not be valid');

		$protectLib = new Protect($storage, $badUserAgent);
		$this->assertFalse($protectLib->isValidRequest($form), 'Other User agent test, token must not be valid');


		//Simulate Expired token
		$saveValidToken = clone $storage['ACSRF_TOKENS'][$tokenkey];
		$storage['ACSRF_TOKENS'][$tokenkey]->validTime = time() - 10;
		$protectLib = new Protect($storage, $serverInfoInital);
		$this->assertFalse($protectLib->isValidRequest($form), 'This token must be expired');

		//Now the valid token
		$storage['ACSRF_TOKENS'][$tokenkey] = $saveValidToken;
		$protectLib = new Protect($storage, $serverInfoInital);
		$this->assertTrue($protectLib->isValidRequest($form), 'This token must be valid');
	}

	/**
	 * In this case, form is generated on a webpage 1 but form-handler is an another page and we want use url protection.
	 * So we must change url in token generation
	 *
	 * Here, following protection is active :
	 * - only valid for url that passed in parameter
	 * - only valid for ip of visitor
	 * - only valid for user agent of visitor
	 * - valid for 10 minutes
	 *
	 * @see testSimpleExample for detailled explanation
	 *
	 */
	public function testWithAnotherUrlToFormSended() {
		//
		// Test initialisation (you must no do this in prod)
		//
		//Simulate intial page info
		$FirstPageInfo = new ServerInfo();
		$FirstPageInfo->ip = '10.10.10.10';
		$FirstPageInfo->url = 'https://ronan.parke/';
		$FirstPageInfo->userAgent = 'console';

		//Simulate second page info
		$secondPageInfo = clone $FirstPageInfo;
		$secondPageInfo->url = 'https://ronan.parke/process.php';

		$storage = [];

		//
		// Part 1 - Before html form generation
		//
		$protectLib = new Protect($storage, $FirstPageInfo);

		//1rst parameter is for restrict to a particular form (not wanted here)
		//In 2nd parameter, we must pass the url of form handler.
		$token = $protectLib->generateToken([], 'https://ronan.parke/process.php');

		//You must include token in html form like this :
		$echo = '<input type="hidden" name="' . key($token) . '" value="' . current($token) . '" />';

		// Below, we create a exemple form request. (In reality, browser make that)
		$tokenkey = key($token);
		$form = [
			$tokenkey => current($token),
			'name' => 'Parke',
			'firstname' => 'Ronan',
			'method' => 'Add Singer'
		];

		//
		// Part 2 - Next page, on receive form data
		//
		/*
		 * If form is sended on the initial page, it's not work :
		 */
		$protectLib = new Protect($storage, $FirstPageInfo);
		$this->assertFalse($protectLib->isValidRequest($form), 'This token must not work on the initial page');

		/*
		 * But in second page, it's working!
		 */
		$protectLib = new Protect($storage, $secondPageInfo);
		$this->assertTrue($protectLib->isValidRequest($form), 'This token must  work on second page');
	}

	/**
	 * In this example, we disable url protection of token
	 *
	 * Here, following protection is active :
	 * - only valid for ip of visitor
	 * - only valid for user agent of visitor
	 * - valid for 10 minutes
	 *
	 * @see testSimpleExample for detailled explanation
	 */
	public function testWithoutUrlProtection() {
		//
		// Test initialisation (you must no do this in prod)
		//
		//Simulate intial page info
		$FirstPageInfo = new ServerInfo();
		$FirstPageInfo->ip = '10.10.10.10';
		$FirstPageInfo->url = 'https://ronan.parke/';
		$FirstPageInfo->userAgent = 'console';

		//Simulate second page info
		$secondPageInfo = clone $FirstPageInfo;
		$secondPageInfo->url = 'https://ronan.parke/process.php';

		$anotherPageInfo = clone $FirstPageInfo;
		$secondPageInfo->url = 'https://ronan.parke/badpage.php';

		$storage = [];

		//
		// Part 1 - Before html form generation
		//
		$protectLib = new Protect($storage, $FirstPageInfo);

		//For desactivate url protection, we must pass an empty parameter in second position
		$token = $protectLib->generateToken([], '');

		//You must include token in html form like this :
		$echo = '<input type="hidden" name="' . key($token) . '" value="' . current($token) . '" />';

		// Below, we create a exemple form request. (In reality, browser make that)
		$tokenkey = key($token);
		$form = [
			$tokenkey => current($token),
			'name' => 'Parke',
			'firstname' => 'Ronan',
			'method' => 'Add Singer'
		];
		$saveValidToken = clone $storage['ACSRF_TOKENS'][$tokenkey];

		//
		// Part 2 - Next page, on receive form data
		//

		/*
		 * If form is sended on the initial page, it's work :
		 */
		$protectLib = new Protect($storage, $FirstPageInfo);
		$this->assertTrue($protectLib->isValidRequest($form), 'This token must work on the initial page');

		/*
		 * In a second page, it's working.
		 */
		$storage['ACSRF_TOKENS'][$tokenkey] = $saveValidToken;
		$protectLib = new Protect($storage, $secondPageInfo);
		$this->assertTrue($protectLib->isValidRequest($form), 'This token must work on second page');

		/*
		 * It's working on all page
		 */
		$storage['ACSRF_TOKENS'][$tokenkey] = $saveValidToken;
		$protectLib = new Protect($storage, $anotherPageInfo);
		$this->assertTrue($protectLib->isValidRequest($form), 'This token must work on all pages');
	}


	/**
	 * In this case, we have multiple form on one page and we want use a different token for each form.
	 * So we must defined an form protection
	 *
	 * Here, following protection is active :
	 * - only valid for 1 form
	 * - only valid for url where form is generated
	 * - only valid for ip of visitor
	 * - only valid for user agent of visitor
	 * - valid for 10 minutes
	 *
	 * @see testSimpleExample for detailled explanation
	 *
	 */
	public function testWithFormRestriction() {
		//
		// Test initialisation (you must no do this in prod)
		//
		//Simulate intial page info
		$FirstPageInfo = new ServerInfo();
		$FirstPageInfo->ip = '10.10.10.10';
		$FirstPageInfo->url = 'https://ronan.parke/';
		$FirstPageInfo->userAgent = 'console';

		$storage = [];

		//
		// Part 1 - Before html form generation
		//
		$protectLib = new Protect($storage, $FirstPageInfo);

		/**
		 * We suppose to have 2 form of this page. For identifing form, we must chose name/value different for each form.
		 * This can be an hidden input type or an submit input type
		 * Here we use name='method' and  value= 'Add Singer' or 'Modify Singer'
		 */
		$token1 = $protectLib->generateToken(['method' => 'Add Singer']);
		$token2 = $protectLib->generateToken(['method' => 'Modify Singer']);

		//HTML example form 1 :
		$tokenkey1 = key($token1);
		$tokenvalue1 = current($token1);
		$htmlform1 = <<<FR1
			<form method="post" action="https://ronan.parke/">
				<input type="text" name="name" />
				<input type="text" name="firstname" />
				<input type="hidden" name="$tokenkey1" value="$tokenvalue1" />
				<input type="submit" name="method" value="Add Singer" />
			</form>
FR1;

		//HTML example form 2 :
		$tokenkey2 = key($token2);
		$tokenvalue2 = current($token2);
		$htmlform2 = <<<FR1
			<form method="post" action="https://ronan.parke/">
				<input type="text" name="name" />
				<input type="text" name="firstname" />
				<input type="hidden" name="$tokenkey2" value="$tokenvalue2" />
				<input type="submit" name="method" value="Modify Singer" />
			</form>
FR1;

		// Below, we create some form request for test
		$formToken1InForm1= [
			$tokenkey1 => $tokenvalue1,
			'name' => 'Parke',
			'firstname' => 'Ronan',
			'method' => 'Add Singer'
		];
		$formToken1InForm2 = [
			$tokenkey1 => $tokenvalue1,
			'name' => 'Parke',
			'firstname' => 'Ronan',
			'method' => 'Modify Singer'
		];
		$formToken2InForm1= [
			$tokenkey2 => $tokenvalue2,
			'name' => 'Parke',
			'firstname' => 'Ronan',
			'method' => 'Add Singer'
		];
		$formToken2InForm2 = [
			$tokenkey2 => $tokenvalue2,
			'name' => 'Parke',
			'firstname' => 'Ronan',
			'method' => 'Modify Singer'
		];

		//
		// Part 2 - Next page, on receive form data
		//
		/*
		 * Token 1 in form 2, it's not work
		 */
		$protectLib = new Protect($storage, $FirstPageInfo);
		$this->assertFalse($protectLib->isValidRequest($formToken1InForm2), 'This token (1) must not work with this form (2)');
		/*
		 * But Token 1 in form 1, it's working!
		 */
		$this->assertTrue($protectLib->isValidRequest($formToken1InForm1), 'This token (1) must work with this form (1)');
		/*
		 * Token 2 in form 1, it's not work
		 */
		$this->assertFalse($protectLib->isValidRequest($formToken2InForm1), 'This token (2) must not work with this form (1)');
		/*
		 * But Token 2 in form 2, it's working!
		 */
		$this->assertTrue($protectLib->isValidRequest($formToken2InForm2), 'This token (1) must work with this form (2)');
	}


	/**
	 * A exemple with a another time duration for token
	 *
	 * Here, following protection is active :
	 * - only valid for url where form is generated
	 * - only valid for ip of visitor
	 * - only valid for user agent of visitor
	 * - valid for you own duretion (5 minutes)
	 *
	 * @see testSimpleExample for detailled explanation
	 *
	 */
	public function testChangeDurationOfAToken() {
		//
		// Test initialisation (you must no do this in prod)
		//
		//Simulate intial page info
		$FirstPageInfo = new ServerInfo();
		$FirstPageInfo->ip = '10.10.10.10';
		$FirstPageInfo->url = 'https://ronan.parke/';
		$FirstPageInfo->userAgent = 'console';

		$storage = [];

		//
		// Part 1 - Before html form generation
		//
		$protectLib = new Protect($storage, $FirstPageInfo);

		//You can change time durection in 3th parameters (5 minutes here)
		$token = $protectLib->generateToken([], Protect::ACTUAL_URL, 300);

		//You must include token in html form like this :
		$echo = '<input type="hidden" name="' . key($token) . '" value="' . current($token) . '" />';


		//Valid time is registered in $storage;
		$tokenkey = key($token);
		$this->assertEquals(300, $storage['ACSRF_TOKENS'][$tokenkey]->validTime - time() , 'La durée du token devrait être de 5 minutes');
	}

	/**
	 * In this example, we disable time protection of token
	 *
	 * Here, following protection is active :
	 * - only valid for url where form is generated
	 * - only valid for ip of visitor
	 * - only valid for user agent of visitor
	 *
	 * @see testSimpleExample for detailled explanation
	 */
	public function testWithoutTimeProtection() {
		//
		// Test initialisation (you must no do this in prod)
		//
		//Simulate intial page info
		$FirstPageInfo = new ServerInfo();
		$FirstPageInfo->ip = '10.10.10.10';
		$FirstPageInfo->url = 'https://ronan.parke/';
		$FirstPageInfo->userAgent = 'console';

		$storage = [];

		//
		// Part 1 - Before html form generation
		//
		$protectLib = new Protect($storage, $FirstPageInfo);

		//Disable time protection is made in option in the 4th parameter
		$token = $protectLib->generateToken([], Protect::ACTUAL_URL, Protect::DEFAULT_VALID_TIME, Protect::RESTRICT_IP | Protect::RESTRICT_USER_AGENT);

		//You must include token in html form like this :
		$echo = '<input type="hidden" name="' . key($token) . '" value="' . current($token) . '" />';

		// Below, we create a exemple form request. (In reality, browser make that)
		$tokenkey = key($token);
		$form = [
			$tokenkey => current($token),
			'name' => 'Parke',
			'firstname' => 'Ronan',
			'method' => 'Add Singer'
		];

		//
		// Part 2 - Next page, on receive form data
		//

		//Simulate 1 day elapsed (not really, but me must do this test)
		$storage['ACSRF_TOKENS'][$tokenkey]->validTime = time() - 3600 * 24;

		$protectLib = new Protect($storage, $FirstPageInfo);
		$this->assertTrue($protectLib->isValidRequest($form), 'This token must be valid');
	}


	/**
	 * In this example, we disable ip protection of token
	 *
	 * Here, following protection is active :
	 * - only valid for url where form is generated
	 * - only valid for user agent of visitor
	 * - valid for 10 minutes
	 *
	 * @see testSimpleExample for detailled explanation
	 */
	public function testWithoutIpProtection() {
		//
		// Test initialisation (you must no do this in prod)
		//
		//Simulate intial page info
		$FirstPageInfo = new ServerInfo();
		$FirstPageInfo->ip = '10.10.10.10';
		$FirstPageInfo->url = 'https://ronan.parke/';
		$FirstPageInfo->userAgent = 'console';

		//Simulate another IP
		$anotheripInfo = clone $FirstPageInfo;
		$anotheripInfo->ip = '20.20.20.20';

		$storage = [];

		//
		// Part 1 - Before html form generation
		//
		$protectLib = new Protect($storage, $FirstPageInfo);

		//For desactivate ip protection, we must change options bit (4th parameters)
		$token = $protectLib->generateToken([], Protect::ACTUAL_URL, Protect::DEFAULT_VALID_TIME, Protect::RESTRICT_USER_AGENT | Protect::RESTRICT_VALID_TIME);

		//You must include token in html form like this :
		$echo = '<input type="hidden" name="' . key($token) . '" value="' . current($token) . '" />';

		// Below, we create a exemple form request. (In reality, browser make that)
		$tokenkey = key($token);
		$form = [
			$tokenkey => current($token),
			'name' => 'Parke',
			'firstname' => 'Ronan',
			'method' => 'Add Singer'
		];
		$saveValidToken = clone $storage['ACSRF_TOKENS'][$tokenkey];

		//
		// Part 2 - Next page, on receive form data
		//

		/*
		 *  With same ip, it's working!
		 */
		$protectLib = new Protect($storage, $FirstPageInfo);
		$this->assertTrue($protectLib->isValidRequest($form), 'This token must work with same ip');

		/*
		 * With Another ip, it's working!
		 */
		$storage['ACSRF_TOKENS'][$tokenkey] = $saveValidToken;
		$protectLib = new Protect($storage, $anotheripInfo);
		$this->assertTrue($protectLib->isValidRequest($form), 'This token must work with another ip');
	}

	/**
	 * In this example, we disable user-agent protection of token.
	 *
	 * Here, following protection is active :
	 * - only valid for url where form is generated
	 * - only valid for ip of visitor
	 * - valid for 10 minutes
	 *
	 * @see testSimpleExample for detailled explanation
	 */
	public function testWithoutUserAgentProtection() {
		//
		// Test initialisation (you must no do this in prod)
		//
		//Simulate intial page info
		$FirstPageInfo = new ServerInfo();
		$FirstPageInfo->ip = '10.10.10.10';
		$FirstPageInfo->url = 'https://ronan.parke/';
		$FirstPageInfo->userAgent = 'console';

		//Simulate another User  Agent
		$anotheripInfo = clone $FirstPageInfo;
		$anotheripInfo->userAgent = 'firefox';

		$storage = [];

		//
		// Part 1 - Before html form generation
		//
		$protectLib = new Protect($storage, $FirstPageInfo);

		//For desactivate user agent protection, we must change options bit (4th parameters)
		$token = $protectLib->generateToken([], Protect::ACTUAL_URL, Protect::DEFAULT_VALID_TIME, Protect::RESTRICT_IP | Protect::RESTRICT_VALID_TIME);

		//You must include token in html form like this :
		$echo = '<input type="hidden" name="' . key($token) . '" value="' . current($token) . '" />';

		// Below, we create a exemple form request. (In reality, browser make that)
		$tokenkey = key($token);
		$form = [
			$tokenkey => current($token),
			'name' => 'Parke',
			'firstname' => 'Ronan',
			'method' => 'Add Singer'
		];
		$saveValidToken = clone $storage['ACSRF_TOKENS'][$tokenkey];

		//
		// Part 2 - Next page, on receive form data
		//

		/*
		 *  With same user-agent, it's working!
		 */
		$protectLib = new Protect($storage, $FirstPageInfo);
		$this->assertTrue($protectLib->isValidRequest($form), 'This token must work with same user-agent');

		/*
		 * With Another user-agent, it's working!
		 */
		$storage['ACSRF_TOKENS'][$tokenkey] = $saveValidToken;
		$protectLib = new Protect($storage, $anotheripInfo);
		$this->assertTrue($protectLib->isValidRequest($form), 'This token must work with another user-agent');
	}
}
?>
