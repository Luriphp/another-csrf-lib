<?php
/**
 * This file is  Part of Another CSRF lib
 *
 * (c) 2020 Luri <luri@e.email>
 *
 ***********************************************************************************************************************
 *                                                       LICENCE
 ***********************************************************************************************************************
 *
 * Another CSRF lib is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the License, or any later version.
 *
 * Another CSRF lib is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with Another CSRF lib.
 * If not, see <https://www.gnu.org/licenses/>.
 *
 ***********************************************************************************************************************
 *
 * Another CSRF lib est un logiciel libre: vous pouvez le redistribuer et / ou le modifier sous les termes de la GNU General Public
 * License comme publié par la Free Software Foundation, version 3 de la licence ou toute version ultérieure.
 *
 * Another CSRF lib est distribué dans l'espoir qu'il sera utile, mais SANS AUCUNE GARANTIE; sans même la garantie implicite de
 * QUALITÉ MARCHANDE ou D'ADÉQUATION À UN USAGE PARTICULIER. Voir la Licence Publique Générale GNU pour plus de détails.
 *
 * Vous devriez avoir reçu une copie de la licence publique générale GNU avec Another CSRF lib.
 * Sinon, voir <https://www.gnu.org/licenses/>.
 *
 ***********************************************************************************************************************
 */
namespace Luri\ACSRFLib\Test;

use PHPUnit\Framework\TestCase;
use PHPUnit\DbUnit\TestCaseTrait;


class UserListTest extends TestCase {

	public function test1() {

	}
}
?>