# Another CSRF lib

[![Latest Version on Packagist][ico-version]][link-packagist]
[![Software License][ico-license]](LICENSE.md)

A small another anti-CSRF lib with simply use.

We generated a token for each form and we validate the token before process an request.

Token is valid one time only. It's can be restricted to one url, one duration, one form, ip & user-agent of visitor.


## Structure

```
src/    For source sode
tests/  For phpunit test code
```


## Install

Via Composer

``` bash
$ composer require luri/anothercsrflib
```

## Usage

In example bellow, following protection is active :
 * only valid for url where form is generated
 * only valid for ip of visitor
 * only valid for user agent of visitor
 * valid for 10 minutes

``` php
use Luri\ACSRFLib\{
	Protect,
	ServerWrapper
};
$protectLib = new Protect($_SESSION, new ServerWrapper());

//
// Part 1 - Before html form generation
//
$token = $protectLib->generateToken();

//You must include the token into html form like this :
echo '<input type="hidden" name="' . key($token) . '" value="' . current($token) . '" />';

//
// Part 2 - on process form data
// (same page here beacause default url protection, but you can change this and use an
// antoher page for process you form data.)
//
if ($protectLib->isValidRequest($_POST)) { // This for example. For security, you must filter and validate user entry.
	//Token is valid, so you can process the request
}
```

You can change or disable URL / time duration.

You can add an form protection

You can disable ip & user agent protection.


See tests/UsageTest.php for more example.

See commentary in src/Protect.php

## Change log

Please see [CHANGELOG](CHANGELOG.md) for more information on what has changed recently.

## Testing

``` bash
$ composer test
```

## Security

If you discover any security related issues, please email luri@e.email instead of using the issue tracker.

## Credits

-  Luri

## License

This program is free software: you can redistribute it and/or modify it under the terms of the [GNU General Public License] (LICENSE.md) as published by the [Free Software Foundation] (https://www.fsf.org/), either version 3 of the License, or any later version.

This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the [GNU General Public License] (LICENSE.md) for more details.

Copyright 2020, Luri & authors credited [bellow] (#Credits).

[ico-license]: https://img.shields.io/badge/licence-GPL%20(%3E%3D3)-brightgreen?style=flat-square
[ico-version]: https://img.shields.io/packagist/v/luri/anothercsrflib.svg?style=flat-square

[link-packagist]: https://packagist.org/packages/luri/anothercsrflib
